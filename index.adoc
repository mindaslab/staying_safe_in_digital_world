= Staying Safe In Digital World
Karthikeyan A K <77minds@gmail.com>
:toc: left

== Preface

image::https://openclipart.org/image/2400px/svg_to_png/164617/full-face-helmet.png[500, 500]

Its appaling that how many people use an digital equipment connected to the Internet, and yet a vast percentage of them don't know what software and information freedom is. Here I don't mean just the non tech people, even technical people seems no to know, and even if they knew they seem not to care about it.

As a patriot of free information and free software, I feel I am failing my duty as a human if I am not making people aware of it, hence I write this book.

== I

I am Karthikeyan A K, just another Engineer from India. I am die hard fan of Free(dom) Computing. That is one must be able to compute witout hinderance, one must toaly own his computing and should be able to trace out what happens insde his machine.

I run my Data Science company http://trumatics.com[Trumatics], and am Free Software http://fsf.og enthusiast and evangilist.

== Copyrights And Getting This Book

I don't belive in Copyrights. You can get this book and its source here https://gitlab.com/mindaslab/staying_safe_in_digital_world .


include::you_are_not_in_control.adoc[]

include::spying_economy.adoc[]

include::searching_safe.adoc[]

include::browse_safe.adoc[]

== Safe Office

=== Libre Office

== Email Safe

=== Thunderbird + Enigmail

=== Mailpile


== Store Safe

=== Spider Oak

=== Next Cloud

=== External Drive

include::watching_videos.adoc[]

== You Have The Right To Share

image::https://i.ytimg.com/vi/WbqFn1oI1tc/maxresdefault.jpg[]

video::eTOKXCEwo_8[youtube, 800, 400]

== Social Networking In Freedom

=== Diaspora

=== Mastadon

== Messaging In Freedom

=== Rumble

=== Signal

=== Ring

== The Free Software Foundation

video::Ag1AKIl_2GM[youtube, 800, 400]

include::operating_system.adoc[]

== Fixing Things

== Hardware That Respects You

=== System 76

=== Purism

== Hosting Your Source Code

=== Gitlab

== The Dark Net

=== ZeroNet

== Taking Control Of The Internet

== Safe Banking

You might trust your bank, they are a good idea, idea where you put your money in a place thats safe, and that safety is guranteed even if the bank gets robbed. But banks wanted to make a lot of profut today, and since they know how tou spend thanks to your credit and debit card information, they have started selling that information to others to make ore profit.

The only way to avoid this is not to use cards which tell the bank where you spend your money. The best way to do  a trade or purchase it with cash, the plain old cash where its difficult to track you.

Make sure that you draw money, quiet large sums (say what you need for a month) from a single ATM, by doing so , bank will not know whwere you travelled. Then spend that money as you would do in old days. You will see spam emails and messages geeting a bit low.

=== Bitcoin

If you really hate banks and how all those white elephants in it are misuing your money, try using Bitcoin. By eliminating the banking middleman, it ensures transactions are fairly cheap. But right now bit coin i really volatile, its value fluctuates widely. To know more about it, you can go here https://bitcoin.org/

